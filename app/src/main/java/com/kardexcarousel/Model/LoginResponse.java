package com.kardexcarousel.Model;

import com.google.gson.annotations.SerializedName;

public class LoginResponse{

	@SerializedName("result")
	private boolean result;

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private LoginData data;

	public boolean isResult(){
		return result;
	}

	public String getMsg(){
		return msg;
	}

	public LoginData getData(){
		return data;
	}
}
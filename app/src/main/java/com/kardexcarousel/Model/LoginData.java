package com.kardexcarousel.Model;

import com.google.gson.annotations.SerializedName;

public class LoginData {

	@SerializedName("userPassword")
	private String userPassword;

	@SerializedName("createdDate")
	private String createdDate;

	@SerializedName("userEmail")
	private String userEmail;

	@SerializedName("updatedDate")
	private Object updatedDate;

	@SerializedName("userName")
	private String userName;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("userID")
	private String userID;

	public String getUserPassword(){
		return userPassword;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public String getUserEmail(){
		return userEmail;
	}

	public Object getUpdatedDate(){
		return updatedDate;
	}

	public String getUserName(){
		return userName;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public String getUserID(){
		return userID;
	}
}